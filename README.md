# Lab - Utilisation du DNS Kubernetes pour Découvrir des Services

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Effectuer une recherche Nslookup pour un service dans le même espace de noms

2. Effectuer une recherche Nslookup pour un service dans un espace de noms différent

# Contexte

Votre entreprise, BeeHive, travaille à l'évolution de son infrastructure d'applications Kubernetes. Actuellement, ils disposent de deux composants d’application dans deux espaces de noms différents : une base de données d’utilisateurs et une interface Web.

Vos développeurs ne savent pas vraiment comment fonctionne le DNS Kubernetes et ont donc du mal à accéder à certains de leurs services. Ils vous ont demandé d'effectuer des tests qui les aideront à confirmer et à clarifier le comportement du DNS Kubernetes.

Un Pod appelé busyboxexiste déjà dans l' webespace de noms. Vous pouvez utiliser ce Pod pour effectuer vos tests. Enregistrez le résultat de vos tests dans certains fichiers, afin que les développeurs puissent voir les résultats

>![Alt text](img/image.png)



# Introduction
Les services Kubernetes peuvent être localisés avec le DNS Kubernetes, tout comme les pods. Dans cet atelier, vous travaillerez avec le DNS Kubernetes pour découvrir des services à partir d'un pod. Cela testera vos connaissances sur la façon d’interagir avec les services en utilisant le DNS.

# Application

## Étape 1 : Connexion au Control Plan

Connectez-vous au control plan

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Effectuer une Recherche Nslookup pour un Service dans le Même Espace de Noms

1. Utilisez le pod `busybox` dans l'espace de noms `web` pour effectuer une recherche DNS sur le service `web-frontend` :

```sh
kubectl exec -n web busybox -- nslookup web-frontend
```

>![Alt text](img/image-1.png)
*Nom de domaine qualifié*

Notre commande à bien fonctionnée, nous pouvons voir le nom de domaine pleinement qualifié du service **web-frontend** s'afficher : **"web-frontend.web.svc.cluster.local"**

2. Redirigez la sortie pour enregistrer les résultats dans un fichier texte :

```sh
kubectl exec -n web busybox -- nslookup web-frontend >> ~/dns_same_namespace_results.txt
```

>![Alt text](img/image-2.png)
*Sauvegarde des données DNS*

3. Recherchez le même service en utilisant le nom de domaine complet :

```sh
kubectl exec -n web busybox -- nslookup web-frontend.web.svc.cluster.local
```
>![Alt text](img/image-3.png)
*Service DNS web-frontend*

4. Redirigez la sortie pour enregistrer les résultats du deuxième nslookup dans un fichier texte :

```sh
kubectl exec -n web busybox -- nslookup web-frontend.web.svc.cluster.local >> ~/dns_same_namespace_results.txt
```

>![Alt text](img/image-4.png)
*Sauvegarde des informations DNS*

Nous pouvons ainsi sauvegarder les informations de nos deux requêtes et plus facilement effectuer une comparaison.

5. Vérifiez que tout semble correct dans le fichier texte :

```sh
cat ~/dns_same_namespace_results.txt
```


## Étape 3 : Effectuer une Recherche Nslookup pour un Service dans un Espace de Noms Différent

1. Utilisez le pod `busybox` dans l'espace de noms `web` pour effectuer une recherche DNS sur le service `user-db` dans l'espace de noms `data` en utilisant uniquement le nom court du service :

```sh
kubectl exec -n web busybox -- nslookup user-db
```

>![Alt text](img/image-5.png)

   - Cette première requête est censée générer un message d'erreur, alors ne vous inquiétez pas si vous voyez que la résolution de `user-db` échoue, car nous essayons de rechercher un service qui se trouve dans un espace de nom différen de celui du pod sur le quel l'on se log pour exécuter les requêtes DNS, et nous utilisson simplement le nom court du services **"user-db"**.

2. Enregistrez les résultats de ce nslookup dans un fichier texte :

```sh
kubectl exec -n web busybox -- nslookup user-db >> ~/dns_different_namespace_results.txt
```

>![Alt text](img/image-6.png)
*Enregistrement des information de collecte DNS*

3. Effectuez la même recherche en utilisant le nom de domaine complet :

```sh
kubectl exec -n web busybox -- nslookup user-db.data.svc.cluster.local
```

>![Alt text](img/image-7.png)
*Sauvegarde des informations DNS*

Cette fois ci, la commande `nslookup` a marché, même si nous recherchions un service dans un espace de noms différent, car nous avons utilisé le nom de domaine pleinement qualifié pour cela.

4. Enregistrez les résultats dans un fichier texte :

```sh
kubectl exec -n web busybox -- nslookup user-db.data.svc.cluster.local >> ~/dns_different_namespace_results.txt
```

>![Alt text](img/image-8.png)

5. Vérifiez la sortie dans le fichier texte :

```sh
cat ~/dns_different_namespace_results.txt
```

Ce laboratoire vous guide à travers le processus d'utilisation du DNS Kubernetes pour découvrir des services dans le même espace de noms et dans des espaces de noms différents, en vérifiant que les services sont correctement résolus par les pods.